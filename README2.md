mise en ligne heroku : https://theatre-romain.herokuapp.com/
gitlab : https://gitlab.com/romain.cadeau31/le-carnet-des-artistes



Contexte du projet

La "société des beaux-parleurs" est un club d'improvisation théatrale, dont les membres se réunissent chaque jeudi soir pour s'adonner à leur pratique préférée. L'impro théatrale se déroule comme suit, sous forme de "battles" : l'arbitre au centre, et deux équipes de 2 ou 3 personnes de chaque côté. L'arbitre annonce un sujet, par exemple :

    Deux familles se croisent au concours de barbecue de Dunkerque

Les deux teams commencent alors à improviser. Parfois, l'arbitre mentionne une contrainte supplémentaire : l'impro se déroule en language "gromlo".

À la fin du battle, une équipe est déclarée gagnante par l'arbitre, qui rend son verdict à l'applaudimètre : en invitant le public à faire un maximum de bruit pour l'équipe qui l'a le plus convaincu !

L'association vous sollicite pour créer un outil en ligne, qui permette à l'ensemble des membres du club de proposer des sujets durant la semaine.

Une page "roulette" permettrait ensuite, le jour J, de tirer au sort les sujets au fil de la soirée (à chaque chargement de page, un sujet aléatoire est affiché).

Petite précision : lorsqu'un sujet s'affiche, il devrait être marqué comme "traité" et ne plus être affiché par la suite !

Il est à noter que l'association dispose d'un hébergement mutualisé chez OVH (offre premier prix) ainsi que d'un nom de domaine.

Facultativement, l'appli pourrait permettre au président du club de saisir les prochains lieux et dates et , ainsi que la liste des adhérents. Tout cela serait donc visible sur le site.
