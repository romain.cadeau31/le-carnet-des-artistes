@extends('layouts.app')

@section('content')

{{-- @if(session()->has('info'))
<div class="notification is-success">
    {{ session('info') }}
</div>
@endif --}}
<table class="table">
    <thead>
        <tr>
            <th scope="col">Lieu</th>
            <th scope="col">Date</th>
        </tr>
    </thead>
    <tbody>
        <?php
            setlocale(LC_TIME, 'french.UTF-8', 'fr_FR.UTF-8');
        ?>
        @foreach ($events as $event)
        <tr>
            <th scope="row">{{ $event->location }}</th>
            <td>{{ strftime("%A %d %B %G", strtotime($event->date))}}</td>
        </tr>
        @endforeach
    </div>
</div>

@endsection
