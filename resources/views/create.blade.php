@extends('layouts.app')

@section('content')
<div class="row justify-content-center">
    <div class="col-md-8">
        <div class="card">
            <div class="card-header">
                <h2 class="text-center">Créer un sujet</h2>
            </div>
            <div class="card-body text-center ">
                <form class=" d-flex flex-column align-items-center" action="{{ route('subjects.store') }}" method="POST">
                    @csrf
                    <div class="field">
                        <label for="FormControlAddSubject">Sujet</label>
                        <div class="control">
                            <textarea class="textarea" name="description" id="formAddSubject" rows="3">{{ old('description') }}</textarea>
                        </div>
                    </div>
                    @error('description')
                    <p class="help is-danger">{{ $message }}</p>
                    @enderror
                    <div class="field">
                    <div class="control">
                        <button style="border-radius: 25px" class="btn btn-primary mr-5">Envoyer</button>
                    </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

@endsection
