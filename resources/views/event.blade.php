@extends('layouts.app')

@section('content')
<div class="row justify-content-center">
    <div class="col-md-8">
        <div class="card">
            <div class="card-header">
                <h2 class="text-center">Créer un event</h2>
            </div>
            <div class="card-body text-center ">
                <form class=" d-flex flex-column align-items-center " action="{{ route('events.store') }}" method="POST">
                    @csrf
                    <div class="field">
                        <label for="FormControlAddEvent ">Lieu</label>
                        <div class="control">
                            <input class="input @error('location') is-danger @enderror" type="text" name="location" value="{{ old('location') }}" placeholder="Lieu de l'évent">
                        </div>
                    </div>
                    @error('location')
                    <p class="help is-danger">{{ $message }}</p>
                    @enderror
                    <label class="mr-4">Date de l'évent</label>
                    <div class="control">
                        <input class="input" type="date" name="date" value="{{ old('date') }}" min="{{ date('Y-m-d') }}">
                    </div>
                    @error('date')
                        <p class="help is-danger">{{ $message }}</p>
                    @enderror
                    <div class="field">
                    <div class="control">
                        <button style="border-radius: 25px" class="btn btn-primary mr-5">Envoyer</button>
                    </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

@endsection
